const express = require('express')
const bodyParser = require('body-parser')
const request = require('request')
const qs = require('querystring')
const fs = require('fs')
const https = require('https')
const http = require('http')

const app = express()

var MongoClient = require('mongodb').MongoClient
var ObjectId = require('mongodb').ObjectID

var url = 'mongodb://52.34.226.223:27017/cs_bot'

app.set('port', (process.env.PORT || 5001))

var options	= {
	key: fs.readFileSync('ssl_cert/private.key'),
	ca: fs.readFileSync('ssl_cert/ca_bundle.crt'),
	cert: fs.readFileSync('ssl_cert/certificate.crt')
}

app.use(bodyParser.urlencoded({extended: false}))

app.use(bodyParser.json())

var server = https.createServer(options, app).listen(app.get('port'), function(){
  console.log("Express server listening on port " + app.get('port'));
})

app.get('/', function (req, res) {
	res.send('Career Sensy Fb Bot Server Started')
})

app.get('/webhook/', function (req, res) {
	if (req.query['hub.verify_token'] === 'verify_me') {
		res.send(req.query['hub.challenge'])
	}
	res.send('Error, wrong token')
})

app.post('/webhook/', function (req, res) {
	var messaging_events = req.body.entry[0].messaging
	for (var i = 0; i < messaging_events.length; i++) {
		var event = req.body.entry[0].messaging[i]
		var sender = event.sender.id
		console.log('Sender : '+sender+' |||||||||||||||||||||||||||||||||||||||||||||||||')
		authUser(sender, event)
	}
	res.sendStatus(200)
})


const token = "EAADaZCeOTsPUBAOb6kMcd0n8k6WLZBzgA89rrT9FzTZAb81Bq0rnD1efbCAQDOHg4jXY15QeGZAZCSNUoQbEZBwAn8ov06JgXoIVq9TbkcbUcO871ZABSlMrBNLnq2kEKxU2qBnJ3qzZBiR0qu4MkJ4WBz2KpqyQjlawMKPUReG0LAZDZD"

function authUser(sender, event){
	MongoClient.connect(url, function(err, db) {
	  	var cursor = db.collection('fb_user_profile').find({ "fb_id": parseInt(sender) }).toArray(function(err, res){
	   	if(res.length != 0){
            eventHandle(sender, event)
         }else{
            request("https://graph.facebook.com/v2.6/"+sender+"?fields=first_name,last_name,profile_pic,locale,timezone,gender&access_token="+token, function(error, response, body) {

               var data = JSON.parse(body)
               if(data.error){
                  sendTextMessage(sender, 'Error authenticating', 'error')
               }else{
                  var cursor = db.collection('fb_user_profile').insertOne({
                     "first_name": data.first_name,
                     "last_name": data.last_name,
                     "profile_pic": data.profile_pic,
                     "locale": data.locale,
                     "timezone": data.timezone,
                     "gender": data.gender,
                     "username": "",
                     "email": "",
                     "mobile": "",
                     "fb_id": parseInt(sender),
                     "time_stamp": Date.now(),
                     "score": {
                     	"R": 0,
                     	"I": 0,
                     	"A": 0,
                     	"S": 0,
                     	"E": 0,
                     	"C": 0
                     }
                  }, function(err){
                     if(err){
                       sendTextMessage(sender, 'Error authenticating', 'error')
                    }else{
                       eventHandle(sender, event)
                    }
                  })
               }
            })
         }
	   })
	})
}

function eventHandle(sender, event){
	MongoClient.connect(url, function(err, db) {
	  	var cursor = db.collection('fb_user_profile').find({ "fb_id": parseInt(sender) }).toArray(function(err, res){
			if (event.message && event.message.quick_reply){
				var text = event.message.quick_reply.payload

				if(text === 'YO'){

					request("http://52.34.226.223:8008/next_response/"+sender+'/YO', function(error, response, body) {
					  var resp = body
					  var a = JSON.parse(resp)

					  var msgData = {
							"text": a.response
						}

					  var sent_msg = text.replace(/\//g, "").toLowerCase()
					  var received_msg = msgData.text
					  var msg_cat = a.msg_cat
					  var time_stamp = Date.now()

					  sendMessage(sender, msgData, sent_msg, received_msg, msg_cat, time_stamp)
					  
					})
					
				}else if(text === 'NO'){
					var msgData = { text: 'Yo can type help to get started'}
					sendPostbackMessage(sender, msgData, 'NO', text.substring(0, 200))
				}

			}else if (event.message && event.message.text) {
				var text = event.message.text
				
				if(text.toLowerCase() == 'hello' || text.toLowerCase() == 'hi' || text.toLowerCase() == 'hey' || text.toLowerCase() == 'hy' || text.toLowerCase() == 'yo' || text.toLowerCase() == 'hey medy'){
					var msgData = {
						"text":"Hey "+res[0].first_name+"!! CS is here to help you decide best career opportunities. Let's get started :-D",
					    "quick_replies":[
					      {
					        "content_type":"text",
					        "title":"Yo",
					        "payload":"YO"
					      },{
					        "content_type":"text",
					        "title":"No",
					        "payload":"NO"
					      }
					    ]
					}

					var sent_msg = text.toLowerCase()
					var received_msg = msgData.text
					var msg_cat = 'GS'
					var time_stamp = Date.now()
					sendMessage(sender, msgData, sent_msg, received_msg, msg_cat, time_stamp)
				}else if(text.toLowerCase() == 'help'){
					var msgData = {
						"text":"Hey "+res[0].first_name+"!! CS is here to help you decide best career opportunities. Let's get started :-D",
					    "quick_replies":[
					      {
					        "content_type":"text",
					        "title":"Yo",
					        "payload":"YO"
					      },{
					        "content_type":"text",
					        "title":"No",
					        "payload":"NO"
					      }
					    ]
					}
					sendPostbackMessage(sender, msgData, 'GS', text.substring(0, 200))
				}else if(text.toLowerCase() == 'test'){
					var msgData = {
					  "attachment": {
					    "type": "template",
					    "payload": {
					      "template_type": "generic",
					      "elements": [
					        {
					          "buttons": [
					            {
					              "url": "http://careersensy.com",
					              "type": "web_url",
					              "title": "Get Details"
					            }
					          ],
					          "image_url": "http://careersensy.com/img/logo.png",
					          "subtitle": "Job Mini Description",
					          "title": "Job Title"
					        }
					      ]
					    }
					  }
					}
					var sent_msg = "test"
				  	var received_msg = 'template'
				  	var msg_cat = 'holland'
				  	var time_stamp = Date.now()
				    sendMessage(sender, msgData, sent_msg, received_msg, msg_cat, time_stamp)
				}else if(text.toLowerCase() == 'my status' || text.toLowerCase() == 'status' || text.toLowerCase() == 'what is my status now'){
					request("http://52.34.226.223:8008/threshold/"+sender, function(error, response, body) {
					  var a = JSON.parse(body)
					  //var msgData = {}
					  if(a.status == 'false'){
					  	var msgData = {
							"text":"Let's interact little more"
						}
						var sent_msg = "my status"
					  	var received_msg = JSON.stringify(a.holland_value)
					  	var msg_cat = 'holland'
					  	var time_stamp = Date.now()
					    sendMessage(sender, msgData, sent_msg, received_msg, msg_cat, time_stamp)
					  }else{
					  	var msgData = { text: a.holland }

					  	request({
							url: 'https://graph.facebook.com/v2.6/me/messages',
							qs: {access_token:token},
							method: 'POST',
							json: {
								recipient: {id:sender},
								message: msgData,
							}
						}, function(error, response, body) {
							if (error) {
								console.log('Error sending messages: ', error)
							} else if (response.body.error) {
								console.log('Error: ', response.body.error)
							}else{
								MongoClient.connect(url, function(err, db) {
	  								var cursor = db.collection('jobs').find({ "h_1": a.holland_value[0], "h_2": a.holland_value[1] }).toArray(function(err, res){
	  									console.log(JSON.stringify(res))
	  									var msgData = {
										  "attachment": {
										    "type": "template",
										    "payload": {
										      "template_type": "generic",
										      "elements": []
										    }
										  }
										}
	  									for(var i=0; i < res[0].jobs.length; i++){
	  										msgData.attachment.payload.elements.push({"buttons": [{"url": "https://www.onetonline.org/link/summary/"+res[0].jobs[i].ID,"type": "web_url","title": "Get Details"},{"type":"postback","payload":"REQ_"+res[0].jobs[i].ID,"title":"Know Skills Required"}],"image_url": "http://www.articlecube.com/sites/default/files/field/image/20906/career-development.jpg","title": res[0].jobs[i].val})
	  									}
	  									/*var msgData = { text: JSON.stringify(res[0].jobs)}*/
										var sent_msg = "my status"
										var received_msg = JSON.stringify(a.holland_value)
										var msg_cat = 'holland'
										var time_stamp = Date.now()
										sendMessage(sender, msgData, sent_msg, received_msg, msg_cat, time_stamp)
	  								})
	  							})
								
							}
						})

					  	
					  }
					  
					})
				}else{
					/*request('http://52.34.226.223:8008/threshold/'+sender, function(error, response, body){
						var r = JSON.parse(body)
						if(r.status == 'ok'){
						  var msgData = { text: r.holland }
						  var sent_msg = "my status"
						  var received_msg = msgData.text
						  var msg_cat = 'holland'
						  var time_stamp = Date.now()

						  sendMessage(sender, msgData, sent_msg, received_msg, msg_cat, time_stamp)
						}else{*/
							request("http://52.34.226.223:8008/bot_new_res/"+sender+'/'+qs.escape(text.replace(/\//g, " ")), function(error, response, body) {
							  var a = JSON.parse(body)

							  var msgData = {}

							  if(a.status == 'ok'){
							  	msgData = { text: a.response}
							  }else{
							  	msgData = { text: "Something went wrong :-("}
							  }

							  var sent_msg = text.replace(/\//g, "").toLowerCase()
							  var received_msg = msgData.text
							  var msg_cat = a.msg_cat
							  var time_stamp = Date.now()

							  sendMessage(sender, msgData, sent_msg, received_msg, msg_cat, time_stamp)
							  
							})
					/*	}
					})*/
					
				}

			}else if (event.postback) {
				var text = event.postback.payload
				if (text == "GET_STARTED"){
					var msgData = {
						"text":"Hey "+res[0].first_name+"!! CS is here to help you decide best career opportunities. Let's get started :-D",
					    "quick_replies":[
					      {
					        "content_type":"text",
					        "title":"Yo",
					        "payload":"YO"
					      },{
					        "content_type":"text",
					        "title":"No",
					        "payload":"NO"
					      }
					    ]
					}
					sendPostbackMessage(sender, msgData, 'GS', text.substring(0, 200))
				}else if(text.split('_')[0] == "REQ"){
					console.log("||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||")
					request("http://52.34.226.223:8008/req_skills/"+text.split('_')[1], function(error, response, body) {
					  var a = JSON.parse(body)

					  var msgData = {}

					  if(a.status == 'ok'){

					  	var res = ''

					  	for(var i=0; i<a.skills.slice(0,5).length; i++ ){
					  		res += a.skills.slice(0,5)[i]+'\n'
					  	}

					  	msgData = { text: res }
					  }else{
					  	msgData = { text: "Something went wrong :-("}
					  }

					  var sent_msg = text.split('_')[1]
					  var received_msg = msgData.text
					  var msg_cat = 'Payload'
					  var time_stamp = Date.now()

					  sendMessage(sender, msgData, sent_msg, received_msg, msg_cat, time_stamp)
					  
					})
				}
				
			}
		})
	})
}

function sendPostbackMessage(sender, msgData, m_cat, text){
	var mData = msgData
	var sent_msg = text.toLowerCase()
	var received_msg = msgData.text
	var msg_cat = m_cat
	var time_stamp = Date.now()
	sendMessage(sender, mData, sent_msg, received_msg, msg_cat, time_stamp)
}

function sendTextMessage(sender, text, msgType) {
	var messageData = { text:text }
	
	request({
		url: 'https://graph.facebook.com/v2.6/me/messages',
		qs: {access_token:token},
		method: 'POST',
		json: {
			recipient: {id:sender},
			message: messageData,
		}
	}, function(error, response, body) {
		if (error) {
			console.log('Error sending messages: ', error)
		} else if (response.body.error) {
			console.log('Error: ', response.body.error)
		}else{
			insertLog(sender, text, text, '', Date.now())
		}
	})
}

function sendMessage(sender, msgData, sent_msg, received_msg, msg_cat, time_stamp){

	request({
		url: 'https://graph.facebook.com/v2.6/me/messages',
		qs: {access_token:token},
		method: 'POST',
		json: {
			recipient: {id:sender},
			message: msgData,
		}
	}, function(error, response, body) {
		if (error) {
			console.log('Error sending messages: ', error)
		} else if (response.body.error) {
			console.log('Error: ', response.body.error)
		}else{
			insertLog(sender, sent_msg, received_msg, msg_cat, time_stamp)
		}
	})
}

function insertLog(sender, sent_msg, received_msg, msg_cat, time_stamp){
	MongoClient.connect(url, function(err, db) {
		var cursor = db.collection('fb_msg_log').insertOne({
			"fb_id": parseInt(sender),
			"sent_msg": sent_msg,
			"received_msg": received_msg,
			"msg_cat": msg_cat,
			"timestamp": time_stamp
		}, function(err){
			if(err){
				console.log('Error logging message')
			}
		})
	})
}